//============================================================================
// Name        : iotivitydevice.cpp
// Author      : RiTa
// Version     : 0.9
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================



#include <RCSResourceObject.h>
#include <RCSRequest.h>
#include <OCPlatform.h>
#include <iotivity_config.h>
#include <OCApi.h>
#include <ocpayload.h>
#include <NSProviderInterface.h>
#include <NSCommon.h>
#include <octypes.h>
#include <ocstack.h>
#include <oic_string.h>
#include "oic_malloc.h"

#include <iostream>
#include <pthread.h>
#include <unistd.h>


using namespace std;
using namespace OC::OCPlatform;
using namespace OIC::Service;

struct CloseApp{};

//Menu field
constexpr int DEVICE_INFO = 1;
constexpr int STATE = 2;
constexpr int SWITCH = 3;
constexpr int INCREASE = 4;
constexpr int DECREASE = 5;
constexpr int QUIT = 6;


// Device field
const std::string ATTR_KEY_NAME = "name";
const std::string NAME = "TemperatureSensor";

const std::string ATTR_KEY_NAME_PRODUCTOR = "nameProductor";
const std::string PRODUCTOR = "RiTa";

const std::string ATTR_KEY_MODEL = "model";
const std::string  MODEL = "Premium";

const std::string ATTR_KEY_ID = "serialNumber";
const std::string ID = "30";

const std::string ATTR_KEY_ISON = "isOn";
bool isOn = true;
string isOnString ="ON";

const std::string ATTR_KEY_POWER_CONSUMPTION = "powerConsumption";
int powerConsumption=50;

const std::string ATTR_KEY_TEMPERATURE = "temperature";
int temperature=20;

//IOTIVITY INFO

// Set of strings for each of platform Info fields
std::string gPlatformId = "0A3E0D6F-DBF5-404E-8719-D6880042463A";
std::string gManufacturerName = "RiTa";
std::string gManufacturerLink = "https://www.iu2dpm.it/it/";
std::string gModelNumber = "UbuntuModel";
std::string gDateOfManufacture = "2018-02-09";
std::string gPlatformVersion = "1.0";
std::string gOperatingSystemVersion = "Ubuntu";
std::string gHardwareVersion = "PC";
std::string gFirmwareVersion = "1.0";
std::string gSupportLink = "https://www.iu2dpm.it/it/";
std::string gSystemTime = "2018-02-09T11.01";

// Set of strings for each of device info fields
std::string  deviceName = "IoTivity Simple Device";
std::string  deviceType = "oic.r.temperature_sensor";
std::string  specVersion = "ocf.1.1.0";
std::vector<std::string> dataModelVersions = {"ocf.res.1.1.0", "ocf.sh.1.1.0"};
std::string  protocolIndependentID = "fa008167-3bbf-4c9d-8604-c9bcb96cb712";

// OCPlatformInfo Contains all the platform info to be stored
OCPlatformInfo platformInfo;

const std::string URI = "/a/TempSensor";
const std::string TYPE = "oic.r.temperature.sensor";
const std::string BASELINE_INTERFACE = "oic.if.baseline";
const std::string ACTUATOR_INTERFACE = "oic.if.a";
const std::string SENSOR_INTERFACE = "oic.if.s";
const std::string CUSTOM_INTERFACE = "test.custom";

typedef void (*DisplayControlMenuFunc)();
//typedef std::function<void()> Run;

//Presence vars
pthread_t mythread;
bool g_isPresenceStarted = false;

//Resource Object var
RCSResourceObject * g_resource;
RCSResourceObject::Ptr res_pointer;

//Notification var
char mainConsumer[37] = {'\0',};
bool providerIsStarted = false;

string getUniqueID(){
    return NAME + PRODUCTOR + MODEL + ID;
}

void printState(){
    cout << ATTR_KEY_ISON <<" : " << isOnString << endl;
    cout << ATTR_KEY_POWER_CONSUMPTION <<" : " << powerConsumption << endl;
    cout << ATTR_KEY_TEMPERATURE <<" : " << temperature << " °C " << endl;
}

void printDeviceInfo(){
    std::cout << ATTR_KEY_NAME <<" : " << NAME << std::endl;
    std::cout << ATTR_KEY_NAME_PRODUCTOR <<" : " << PRODUCTOR << std::endl;
    std::cout << ATTR_KEY_MODEL <<" : " << MODEL << std::endl;
    std::cout << ATTR_KEY_ID <<" : " << ID << std::endl;
    printState();
}

int processUserInput(int min, int max)
{
    assert(min <= max);

    int input = 0;

    std::cin >> input;

    if (!std::cin.fail())
    {
        if (input == max + 1)
        {
            throw CloseApp();
        }
        if (min <= input && input <= max)
        {
            return input;
        }
    }

    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    throw std::runtime_error("Invalid Input, please try again");
}

void displayControlTemperatureMenu()
{
    std::cout << "========================================================\n";
    std::cout << DEVICE_INFO << ". Show Device Information                \n";
    std::cout << STATE <<       ". Show Device State                      \n";
    std::cout << SWITCH <<      ". Switch Device On/Off                   \n";
    std::cout << INCREASE <<    ". Increase Temperature by 1 degree       \n";
    std::cout << DECREASE <<    ". Decrease Temperature by 1 degree       \n";
    std::cout << QUIT <<        ". Quit                                   \n";
    std::cout << "========================================================\n";
}


void printAttributes(const RCSResourceAttributes& attrs)
{
    for (const auto& attr : attrs)
    {
        std::cout << "\tkey : " << attr.key() << "\n\tvalue : "
                  << attr.value().toString() << std::endl;
    }
}


void *runPresence(void *param)
{
    g_isPresenceStarted = true;
    while(g_isPresenceStarted){
        startPresence(60);
        cout << "Device notifies his presence" << endl;
        sleep(30);
    }
    return NULL;
}


void notifyPresence()
{
    pthread_create(&mythread,NULL,runPresence,NULL);
    //waiting for end of myhread
    //pthread_join(mythread, NULL);
}

OC::OCRepresentation getOCRepresentation()
{
    string isOnValueString= (isOn == true ) ? "true" : "false";
    ostringstream convert;
    convert << powerConsumption;
    string pow_cons_string = convert.str();
    convert.str("");
    convert << temperature;
    string temperature_string = convert.str();

    OC::OCRepresentation representation = OC::OCRepresentation();
    representation.setValue(ATTR_KEY_ISON,isOnValueString);
    representation.setValue(ATTR_KEY_POWER_CONSUMPTION,pow_cons_string);
    representation.setValue(ATTR_KEY_TEMPERATURE,temperature_string);
    return representation;
}

void publishState()
{
    if(providerIsStarted)
    {
        cout << "Notification state ... ";
        /*char title[100] = {'\0',};
        char body[100] = {'\0',};
        char topic[100] = {'\0',};*/

        string uniqueID = getUniqueID();
        string content = uniqueID + " changes state";

        NSMessage * msg = NSCreateMessage();
        if(msg)
        {
            char * date = "Now";
            char * mediaMessage = "daasd";
            NSMediaContents media = NSMediaContents();
            media.iconImage = mediaMessage;
            msg->title = OICStrdup("TemperatureSensor Reading");
            msg->contentText = OICStrdup(content.c_str());
            msg->sourceName = OICStrdup("Temperature Sensor");
            msg->topic = OICStrdup(uniqueID.c_str());
            msg->ttl = 10;
            msg->dateTime = date;
            msg->mediaContents = &media;
            msg->extraInfo = getOCRepresentation().getPayload();
            NSSendMessage(msg);
            cout << " Sent complete" << endl;
        }
        else
        {
            cout << "Error to create message" << endl;
        }
    }
}

void registerTopic()
{
    string uniqueID = getUniqueID();
    NSProviderRegisterTopic(uniqueID.c_str());
    cout << "Registering Topic : " << uniqueID << endl;
}

void subscribeRequestCallback(NSConsumer *consumer)
{

    cout << " Consumer Device ID: " << consumer->consumerId << " requests to subscribe" << endl;

    if(mainConsumer[0] == '\0')
    {
        OICStrcpy(mainConsumer, 37, consumer->consumerId);
        cout << "mainConsumer: " << mainConsumer << endl;
    }

    NSAcceptSubscription(consumer->consumerId, true);
    NSProviderSetConsumerTopic(consumer->consumerId, getUniqueID().c_str());
    providerIsStarted = true;
}

void syncCallback(NSSyncInfo *sync)
{
    cout << "Sync request : Sync State --> " << sync->state << endl;
}



void startProvider()
{
    cout << "Start Provider Service ..." << endl;
    NSProviderConfig config;
    config.subControllability = true;
    config.subRequestCallback = subscribeRequestCallback;
    config.syncInfoCallback = syncCallback;
    config.userInfo = OICStrdup("OCF_NOTIFICATION");
    NSStartProvider(config);
    sleep(1);
    registerTopic();

}


RCSGetResponse requestHandlerForGet(const RCSRequest & req, RCSResourceAttributes& attrs)
{
    //NON VENGONO EFFETTUATE RICHIESTE GET DA ANDROID THINGS BRAIN

    std::cout << "Received a Get request from Client" << std::endl;
    printAttributes(attrs);
    //TODO ERRORE: RCSResourceObject::LockGuard lock(g_resource);
    //RCSResourceObject::LockGuard lock(res_pointer);
    //g_resource = &(*res_pointer);
    return RCSGetResponse::defaultAction();

}

void switchOnOff(bool value){
    isOn=value;
    powerConsumption= (isOn == true) ? 50 : 5 ;
    isOnString = (isOn == true ) ? "ON" : "OFF" ;
    cout << "----- Switch " << isOnString << " -----" << endl;
}

void setTemperature(int value){
    temperature = value;
    cout << "----- Temperature is -----> " << temperature << " °C " << endl;
}


void parseCommand(string command,RCSResourceAttributes& parameters)
{
    if(command.compare("onOff")==0){
        bool newState = ((parameters["onOffState"].get<string>()).compare("true")==0) ? true : false;
        switchOnOff(newState);
    }
    else if(command.compare("setTemperature")==0){
        cout << "I'm setting temperature";
        int newTemperature = atoi((parameters["temperature"].get<string>()).c_str());
        cout << "new Temperature" << newTemperature;
        setTemperature(newTemperature);
    }
    else if(command.compare("resetTemperature")==0)
            setTemperature(17);
    else cout << "Command not found" << endl;
}



RCSSetResponse requestHandlerForSet(const RCSRequest&, RCSResourceAttributes& attrs)
{
    std::cout << "Received a Set request from Client" << std::endl;
    printAttributes(attrs);
    if(attrs.contains("ACK_CREATION")){
        if(attrs["ACK_CREATION"].get<bool>()){
            startProvider();
            notifyPresence();
        }
    }
    else if (attrs.contains("COMMAND")){
        string command= attrs["COMMAND"].get<string>();
        //attrs.remove("COMMAND");
        try
        {
           parseCommand(command,attrs);
           publishState();
        }
        catch (... ){
           cout << "Errore nel parsing del comando ricevuto " << endl;
        }
    }

    return RCSSetResponse::defaultAction();
}

void initServer()
{
    res_pointer = RCSResourceObject::Builder(URI,TYPE , ACTUATOR_INTERFACE)
            .addInterface(CUSTOM_INTERFACE)
            .addInterface(SENSOR_INTERFACE)
            .setDefaultInterface(BASELINE_INTERFACE)
            .setDiscoverable(true)
            .setObservable(true)
            .build();

    RCSResourceObject::LockGuard lock(res_pointer);

    g_resource = &(*res_pointer);

    g_resource->setAutoNotifyPolicy(RCSResourceObject::AutoNotifyPolicy::UPDATED);

    g_resource->setSetRequestHandlerPolicy(RCSResourceObject::SetRequestHandlerPolicy::ACCEPTANCE);

    string isOnValueString= (isOn == true ) ? "true" : "false";

    ostringstream convert;
    convert << powerConsumption;
    string pow_cons_string = convert.str();
    convert.str("");
    convert << temperature;
    string temperature_string = convert.str();

    g_resource->setAttribute(ATTR_KEY_NAME,RCSResourceAttributes::Value(NAME));
    g_resource->setAttribute(ATTR_KEY_NAME_PRODUCTOR,RCSResourceAttributes::Value(PRODUCTOR));
    g_resource->setAttribute(ATTR_KEY_MODEL,RCSResourceAttributes::Value(MODEL));
    g_resource->setAttribute(ATTR_KEY_ID,RCSResourceAttributes::Value(ID));
    g_resource->setAttribute(ATTR_KEY_ISON,RCSResourceAttributes::Value(isOnValueString));
    g_resource->setAttribute(ATTR_KEY_POWER_CONSUMPTION,RCSResourceAttributes::Value(pow_cons_string));
    g_resource->setAttribute(ATTR_KEY_TEMPERATURE,RCSResourceAttributes::Value(temperature_string));
    g_resource->setAttribute("setTemperature","Imposta la temperatura selezionata-temperature,Integer,10,30,SeekBar");
    g_resource->setAttribute("resetTemperature","Resetta la temperatura di defeault [17 °C]");


    //printAttributes(g_resource->getAttributes());
    //NON ERRORE A RUNTIME---> BHOOOO
    g_resource->setGetRequestHandler(requestHandlerForGet);
    g_resource->setSetRequestHandler(requestHandlerForSet);

}

/*void updateAttribute(const std::string& attrKey, int control)
{
    const int diff = control == INCREASE ? 1 : - 1;

    {
        //RCSResourceObject::LockGuard lock(g_resource);
        auto& attrs = g_resource->getAttributes();
        attrs[attrKey] = attrs[attrKey].get<int>() + diff;
    }

    if (control == INCREASE)
    {
        std::cout << attrKey << " increased." << std::endl;
    }
    else
    {
        std::cout << attrKey << " decreased." << std::endl;
    }
    std::cout << "\nCurrent " << attrKey << ": "<< g_resource->getAttributeValue(attrKey).get<int>();
}

void runResourceControl(DisplayControlMenuFunc displayMenuFunc, const std::string& attrKey)
{
    displayControlTemperatureMenu();
    //updateAttribute(attrKey, processUserInput(INCREASE, DECREASE));
}*/


void runMenuSelection(int menuChoose)
{
    switch (menuChoose)
    {
        case DEVICE_INFO:
            printDeviceInfo();
            break;

        case STATE:
            printState();
            break;

        case SWITCH:
            switchOnOff(!isOn);
            publishState();
            break;

        case INCREASE:
            setTemperature(temperature+1);
            publishState();
            break;

        case DECREASE:
            setTemperature(temperature-1);
            publishState();
            break;
    }


    //g_currentRun = std::bind(runResourceControl, displayMenuFunc, std::move(attrKey));
}



void DeletePlatformInfo()
{
    delete[] platformInfo.platformID;
    delete[] platformInfo.manufacturerName;
    delete[] platformInfo.manufacturerUrl;
    delete[] platformInfo.modelNumber;
    delete[] platformInfo.dateOfManufacture;
    delete[] platformInfo.platformVersion;
    delete[] platformInfo.operatingSystemVersion;
    delete[] platformInfo.hardwareVersion;
    delete[] platformInfo.firmwareVersion;
    delete[] platformInfo.supportUrl;
    delete[] platformInfo.systemTime;
}

void DuplicateString(char ** targetString, std::string sourceString)
{
    *targetString = new char[sourceString.length() + 1];
    strncpy(*targetString, sourceString.c_str(), (sourceString.length() + 1));
}

OCStackResult SetPlatformInfo(std::string platformID, std::string manufacturerName,
        std::string manufacturerUrl, std::string modelNumber, std::string dateOfManufacture,
        std::string platformVersion, std::string operatingSystemVersion,
        std::string hardwareVersion, std::string firmwareVersion, std::string supportUrl,
        std::string systemTime)
{
    DuplicateString(&platformInfo.platformID, platformID);
    DuplicateString(&platformInfo.manufacturerName, manufacturerName);
    DuplicateString(&platformInfo.manufacturerUrl, manufacturerUrl);
    DuplicateString(&platformInfo.modelNumber, modelNumber);
    DuplicateString(&platformInfo.dateOfManufacture, dateOfManufacture);
    DuplicateString(&platformInfo.platformVersion, platformVersion);
    DuplicateString(&platformInfo.operatingSystemVersion, operatingSystemVersion);
    DuplicateString(&platformInfo.hardwareVersion, hardwareVersion);
    DuplicateString(&platformInfo.firmwareVersion, firmwareVersion);
    DuplicateString(&platformInfo.supportUrl, supportUrl);
    DuplicateString(&platformInfo.systemTime, systemTime);

    return OC_STACK_OK;
}

int platformConfig(){

    //OCPersistentStorage ps {client_open, fread, fwrite, fclose, unlink };
    // Create PlatformConfig object
    OC::PlatformConfig cfg {
        OC::ServiceType::InProc,
        OC::ModeType::Server,
        nullptr
    };

    cfg.transportType = static_cast<OCTransportAdapter>(OCTransportAdapter::OC_ADAPTER_IP |
                                                        OCTransportAdapter::OC_ADAPTER_TCP);
    cfg.QoS = OC::QualityOfService::LowQos;

    OC::OCPlatform::Configure(cfg);
    OC_VERIFY(OC::OCPlatform::start() == OC_STACK_OK);
    std::cout << "Starting server & setting platform info\n";

    OCStackResult result = SetPlatformInfo(gPlatformId, gManufacturerName, gManufacturerLink,
                gModelNumber, gDateOfManufacture, gPlatformVersion, gOperatingSystemVersion,
                gHardwareVersion, gFirmwareVersion, gSupportLink, gSystemTime);

    result = OC::OCPlatform::registerPlatformInfo(platformInfo);

    if (result != OC_STACK_OK)
    {
        std::cout << "Platform Registration failed\n";
        return -1;
    }

    return result;
}




int main() {

	cout << "Start Running Temperature Device" << endl;
	printDeviceInfo();
	int input=1;

	int result=platformConfig();
	if (result == OC_STACK_OK){
	    cout << "Platform registration OK"<< endl;
	}

	initServer();


    while (input >0 && input<6)
    {
        displayControlTemperatureMenu();
        try
        {
            input= processUserInput(DEVICE_INFO,QUIT);
            runMenuSelection(input);
        }
        catch(...)
        {
            std::cout << "Uncorrect input... reinsert" << std::endl;
            input = 1;
        }

    }

    //RESET OF THE RESOURCE OBJECT
    if(res_pointer!=nullptr){
        try{
           res_pointer.reset();
        }catch(...){
            cout << "Error to delete ResourceObject" << endl;
        }
    }


    //Stop device presence
    if (g_isPresenceStarted)
    {
        try
        {
            stopPresence();
            std::cout << "Stop device presence" << std::endl;
        }
        catch(...)
        {
            std::cout << "presence stop fail" << std::endl;
        }
    }
    g_isPresenceStarted=false;
    //TODO waiting for end of myhread or go to end ??
    //pthread_join(mythread, NULL);
    DeletePlatformInfo();
    OC_VERIFY(OC::OCPlatform::stop() == OC_STACK_OK);
    cout << "Unbinding platform... " << endl << "Stop device" << endl;
	return 0;
}
